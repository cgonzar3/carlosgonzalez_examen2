<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of HotelModel
 *
 * @author a023948058k
 */

namespace App\Models;
use CodeIgniter\Model;

class HotelModel extends Model {
   
    protected $table = 'hoteles';
    protected $primayKey = 'id';
    protected $returnType = 'object';    
    protected $allowedFields = ['nombre','descripcion','localidad','direccion','cp','email'];
    
    #Para opciones de validacion, mirar /Source Files/system/Language/en/Validation.php
    #Validación:
    protected $validationRules = [
        'nombre'       =>'required|is_unique[hoteles.nombre]',
        'email'    =>'valid_email',
    ];
    
}
