<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('plantillas/plantilla1')?>

<?= $this->section('HEAD') ?>
    <?= $titulo ?>
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
    <!--INICIO TABLA-->
    <div class="container-fluid">
            <h1 class="text-primary"><?= $titulo?></h1>
            
            <a class="btn btn-primary mt-3 mb-4" href="<?= site_url('/hoteles/insertar') ?>">Insertar nuevo Hotel</a>
            
            <table class="table table-striped" id="myTable">
                <thead>
                <tr>                  
                    <th>
                        Nombre
                    </th>
                    <th>
                        Descripcion
                    </th>
                    <th>
                        Dirección
                    </th>
                    <th>
                        Correo electrónico
                    </th>
                    <th>
                        <i>Acciones</i>
                    </th>
                    
                </tr>
                </thead>
                <tbody>
                <?php foreach ($SELECThoteles as $hotel): ?>
                <tr>
                    <td>
                        <?= $hotel->nombre ?>
                    </td>
                    <td style="width: 50%;">
                        <div stile="width:200px;border: solid black 2px">
                            <?= $hotel->descripcion ?>
                        </div>
                    </td>
                    <td>
                        <?= $hotel->direccion ?>, <?= $hotel->cp ?> <?= $hotel->localidad?>
                    </td> 
                    <td>
                        <?= $hotel->email ?>
                    </td>    
                    <td>
                        
                        <a href="<?=site_url('/hoteles/editar/'.$hotel->id)?>" title="Editar Hotel <?= $hotel->nombre?>">
                            <span class="bi bi-pen-fill"></span>
                        </a>                        
                        <a href="<?=site_url('/hoteles/borrar/'.$hotel->id)?>" title="Borrar Hotel <?= $hotel->nombre?>">
                            <span class="bi bi-trash-fill"></span>
                        </a>
                        
                    </td>

                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            
        </div>
    

    <!--FIN TABLA-->
<?= $this->endSection('BODY') ?>