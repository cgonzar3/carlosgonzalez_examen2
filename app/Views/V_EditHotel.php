<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('HEAD') ?>
    <?= $titulo?>
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
    
            <!-- Comprobación de errores -->
            <?php if (!empty($errores)): ?>
                <div class="alert alert-danger">
                    <?php foreach ($errores as $field => $un_error): ?>
                        <p><?= $field ?>: <?= $un_error ?></p>
                    <?php endforeach ?>
                </div>
            <?php endif ?>

    <!--INICIO TABLA-->
    <div class="container-fluid">
        <h1 class="text-primary m-2 mt-3"><?= $titulo?></h1>
        
        <!--<pre>
            <?= print_r($hotel_editado) ?>
        </pre>
        -->
        
        <?= form_open('hoteles/editar/'.$id_hotel_a_editar )?>
        
            <?= form_label('Nombre: ','nombre', ['class'=>'col-sm-2 col-form-label']) ?>
            <?= form_input('nombre',$hotel_editado->nombre,'id="nombre"') ?>
            <br>
            <?= form_label('Descripcion: ','descripcion', ['class'=>'col-sm-2 col-form-label']) ?>
            <?= form_input('descripcion',$hotel_editado->descripcion,'id="descripcion"') ?>
            <br>
            <?= form_label('Localidad:','localidad', ['class'=>'col-sm-2 col-form-label']) ?>
            <?= form_input('localidad',$hotel_editado->localidad,'id="localidad"') ?>
            <br>
            <?= form_label('Dirección: ','direccion', ['class'=>'col-sm-2 col-form-label']) ?>
            <?= form_input('direccion',$hotel_editado->direccion,'id="direccion"') ?>
            <br>
            <?= form_label('Código Postal: ','cp', ['class'=>'col-sm-2 col-form-label']) ?>
            <?= form_input('cp',$hotel_editado->cp,'id="cp"','number') ?>
            <br>
            <?= form_label('Correo electrónico: ','email', ['class'=>'col-sm-2 col-form-label']) ?>
            <?= form_input('email',$hotel_editado->email,'id="email"','email') ?>
            <br>

            
            <?= form_submit('boton_submit','Guardar',['class'=>'btn btn-primary m-3']) ?>
        
        <?= form_close() ?>
        
    </div>
        <a href="<?=site_url('/hoteles')?>" class="btn btn-warning ml-4">
        Volver a Lista Hoteles
    </a>
    <!--FIN TABLA-->
<?= $this->endSection('BODY') ?>