<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */
/**
 * Description of HotelController
 *
 * @author a023948058k
 */

namespace App\Controllers;

class HotelController extends BaseController {
    
    public function Hoteles(){
        
        $HotelModel = new \App\Models\HotelModel;
        $array['SELECThoteles'] = $HotelModel->findAll();
        $array['titulo'] = "Lista de Hoteles";
        return view('V_hoteles',$array);
        
    }
    
        public function HotelesInsertar(){
        
        if (strtolower($this->request->getMethod()) !== 'post'){
            
            $HotelModel = new \App\Models\HotelModel;
            $array['SELECThoteles'] = $HotelModel->findAll();
            $array['titulo'] = "Insertar nuevo hotel";

            helper('form');
            return view('V_InsertHotel',$array);    
            
        } else {
            
            $nuevo_hotel = $this->request->getPost();
            unset($nuevo_hotel['boton_submit']);
            #echo "<pre>";
            #print_r($nuevo_hotel);
            #echo "</pre>";
            
            $HotelModel = new \App\Models\HotelModel;
            
            if ($HotelModel->insert($nuevo_hotel) === false){
               $datos['errores'] = $HotelModel->errors();
               
                        helper('form');
                        $datos['titulo'] = "Insertar nuevo hotel";
               
               return view('V_InsertHotel',$datos); 
            }else {
                return redirect('hoteles');
            }
            
            
            
        }
    }
    
        public function HotelesBorrar($id_hotel_a_borrar){
        
        $HotelModel = new \App\Models\HotelModel;
        $HotelModel->delete(['id' => $id_hotel_a_borrar]);
        $array['titulo'] = "Insertar nuevo hotel";
        
        return redirect('hoteles');
        
    }
    
        public function HotelesEditar($id_hotel_a_editar){
        
        if (strtolower($this->request->getMethod()) !== 'post'){
            
            helper('form');
            $HotelModel = new \App\Models\HotelModel();
            # guardo en |V|         los datos del grupo cuya id es |V|
            $array['hotel_editado'] = $HotelModel->find($id_hotel_a_editar);
            $array['titulo']="Editar Hotel $id_hotel_a_editar";
            $array['id_hotel_a_editar']=$id_hotel_a_editar;
            return view('V_EditHotel',$array);
            
        } else {
                      
            $hotel_datos_nuevos = $this->request->getPost();
            unset($hotel_datos_nuevos['boton_submit']);
            #echo "<pre>";
            #print_r($grupo_datos_nuevos);
            #echo "</pre>";

            $HotelModel = new \App\Models\HotelModel();
            $HotelModel->update($id_hotel_a_editar,$hotel_datos_nuevos);
            $accion = $HotelModel->update($id_hotel_a_editar,$hotel_datos_nuevos);
            
            if ($accion === false){
                #Si no se realiza la accion, devolver Vista con errores
                $array['errores'] = $HotelModel->errors();
                
                        helper('form');
                        $HotelModel = new \App\Models\HotelModel();
                        $array['hotel_editado'] = $HotelModel->find($id_hotel_a_editar);
                        $array['titulo']="Editar Hotel $id_hotel_a_editar";
                        $array['id_hotel_a_editar']=$id_hotel_a_editar;
                        return view('V_EditHotel',$array);
                        
                return view('2aEV/repaso/V_EditGrupo',$array);
            } #si no hay errores, return redirect('repaso');
            
            return redirect('hoteles');
        }
        
    }
    
    
}
